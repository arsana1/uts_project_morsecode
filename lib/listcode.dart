import 'package:flutter/material.dart';
import 'libmorse.dart';

class Listcode extends StatelessWidget {
  const Listcode({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text("List Kode Morse"),
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
          margin: EdgeInsets.only(top: 20, left: 10, right: 10),
          child: ListView(
            children: [
              Text("A : " + "•−", style: TextStyle(fontSize: 30),),
              Text("B : " + "−•••", style: TextStyle(fontSize: 30),),
              Text("C : " + "−•−•", style: TextStyle(fontSize: 30),),
              Text("D : " + "−••", style: TextStyle(fontSize: 30),),
              Text("E : " + "•", style: TextStyle(fontSize: 30),),
              Text("F : " + "••−•", style: TextStyle(fontSize: 30),),
              Text("G : " + "−−•", style: TextStyle(fontSize: 30),),
              Text("H : " + "••••", style: TextStyle(fontSize: 30),),
              Text("I : " + "••", style: TextStyle(fontSize: 30),),
              Text("J : " + "•−−−", style: TextStyle(fontSize: 30),),
              Text("K : " + "−•−", style: TextStyle(fontSize: 30),),
              Text("L : " + "•−••", style: TextStyle(fontSize: 30),),
              Text("M : " + "−−", style: TextStyle(fontSize: 30),),
              Text("N : " + "−•", style: TextStyle(fontSize: 30),),
              Text("O : " + "−−−", style: TextStyle(fontSize: 30),),
              Text("P : " + "•−−•", style: TextStyle(fontSize: 30),),
              Text("Q : " + "−−•−", style: TextStyle(fontSize: 30),),
              Text("R : " + "•−•", style: TextStyle(fontSize: 30),),
              Text("S : " + "•••", style: TextStyle(fontSize: 30),),
              Text("T : " + "−", style: TextStyle(fontSize: 30),),
              Text("U : " + "••−", style: TextStyle(fontSize: 30),),
              Text("V : " + "•••−", style: TextStyle(fontSize: 30),),
              Text("W : " + "•−−", style: TextStyle(fontSize: 30),),
              Text("X : " + "−••−", style: TextStyle(fontSize: 30),),
              Text("Y : " + "−•−−", style: TextStyle(fontSize: 30),),
              Text("Z : " + "−−••", style: TextStyle(fontSize: 30),),
              Text("1 : " + "•−−−−", style: TextStyle(fontSize: 30),),
              Text("2 : " + "••−−−", style: TextStyle(fontSize: 30),),
              Text("3 : " + "•••−−", style: TextStyle(fontSize: 30),),
              Text("4 : " + "••••−", style: TextStyle(fontSize: 30),),
              Text("5 : " + "•••••", style: TextStyle(fontSize: 30),),
              Text("6 : " + "−••••", style: TextStyle(fontSize: 30),),
              Text("7 : " + "−−•••", style: TextStyle(fontSize: 30),),
              Text("8 : " + "−−−••", style: TextStyle(fontSize: 30),),
              Text("9 : " + "−−−−•", style: TextStyle(fontSize: 30),),
              Text("0 : " + "−−−−−", style: TextStyle(fontSize: 30),),
            ],
          ),
        ),
      ),
    );
  }
}
