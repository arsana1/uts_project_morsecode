import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'libmorse.dart';
import 'listcode.dart';
import 'profil.dart';

class InputApp extends StatefulWidget {
  InputApp({Key key}) : super(key: key);

  @override
  _InputAppState createState() => _InputAppState();
}

class _InputAppState extends State<InputApp> {
  String input = " ";
  String output = " ";
  bool modeEncode = true;
  bool modeDecode = false;
  String mode = "Encode";

  final TextEditingController _textField = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Morse Code"),
        actions: [
          IconButton(
            icon: Icon(
              Icons.list,
              color: Colors.white,
            ),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => 
                  Listcode()
                ,),
              );
            },
          ),
          IconButton(
            icon: Icon(
              Icons.account_circle,
              color: Colors.white,
            ),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Profil())
              );
            },
          ), 
        ],
      ),
      body: Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: ListView(
          children: <Widget>[
            Container(
              child: Row(
                children: [
                  Container(
                    child: RaisedButton(
                      onPressed: () {
                        setState(() {
                          _textField.clear();
                          output = " ";
                          modeDecode = false;
                          modeEncode = true;
                          mode = "Encode";
                        });
                      },
                      child: Text("Encode"),
                    ),
                  ),
                  SizedBox(width: 5),
                  Container(
                    child: RaisedButton(
                      onPressed: () {
                        setState(() {
                          _textField.clear();
                          output = " ";
                          modeEncode = false;
                          modeDecode = true;
                          mode = "Decode";
                        });
                      },
                      child: Text("Decode"),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10),
              child: Text("Mode $mode"),
            ),
            Text(
              "Input",
              style: TextStyle(fontSize: 30),
            ),
            Container(
              height: 200,
              child: TextField(
                controller: _textField,
                readOnly: modeDecode,
                maxLines: 7,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                ),
                onChanged: (value) {
                  setState(() {
                    input = value;
                    output = Morse.encode(input);
                  });
                },
              ),
            ),
            Container(
              child: Text(
                "Output : ",
                style: TextStyle(
                  fontSize: 30,
                ),
              ),
            ),
            Container(
              child: Text(
                output,
                style: TextStyle(fontSize: 18),
              ),
              padding: EdgeInsets.all(5),
              height: 200,
              decoration: BoxDecoration(border: Border.all(color: Colors.grey)),
            ),
            Container(
              child: //Text("Button : " + Morse.decode("••••• ")),
                  FlatButton(
                child:
                    Text("Copy Output", style: TextStyle(color: Colors.white)),
                color: Colors.blueAccent,
                onPressed: () {
                  Clipboard.setData(ClipboardData(text: output));
                },
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: Visibility(
        visible: modeDecode,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            FloatingActionButton(
              child: Icon(Icons.space_bar),
              onPressed: () {
                setState(() {
                  _textField.text = _textField.text + " ";
                  output = Morse.decode(_textField.text);
                });
              },
            ),
            FloatingActionButton(
              child: Text(
                "•",
                style: TextStyle(
                  fontSize: 50,
                ),
              ),
              onPressed: () {
                setState(() {
                  _textField.text = _textField.text + "•";
                  output = Morse.decode(_textField.text);
                });
              },
            ),
            FloatingActionButton(
              child: Text(
                "−",
                style: TextStyle(fontSize: 50),
              ),
              onPressed: () {
                setState(() {
                  _textField.text = _textField.text + "−";
                  output = Morse.decode(_textField.text);
                });
              },
            ),
            FloatingActionButton(
              child: Icon(Icons.backspace),
              onPressed: () {
                setState(() {
                  _textField.text = _textField.text.substring(0, _textField.text.length - 1);
                  output = Morse.decode(_textField.text);
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
