import 'package:flutter/material.dart';
import 'libmorse.dart';
import 'input.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: InputApp(),
    );
  }
}

class BodyApp extends StatefulWidget {
  BodyApp({Key key}) : super(key: key);

  @override
  _BodyAppState createState() => _BodyAppState();
}

class _BodyAppState extends State<BodyApp> {
  @override
  
  String textInput; 
  Widget build(BuildContext context) {
    return Container( 
      child: ListView(
        children: <Widget>[
          Container(
            child: TextField(
              onChanged: (value) {
                setState(() {
                  textInput = value;
                });
              },
            ),
          ),
          Container(
            child: Text(
              "Output : "+Morse.encode(textInput),
              style: TextStyle(
                fontSize: 30,
              ),
            ),
          ),
          Container(
            child: Text("Button : "+ Morse.decode("••••• ") ),
          ),
        ],
      ),
    );
  }
}
