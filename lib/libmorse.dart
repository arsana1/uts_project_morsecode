import 'package:flutter/foundation.dart';
// 1 = titik
// 0 = garis

class Morse {
  static String dot = "•";
  static String line = "−";

  static Map<String, List<int>> _morseChar = {
    "A": [1, 0],
    "B": [0, 1, 1, 1],
    "C": [0, 1, 0, 1],
    "D": [0, 1, 1],
    "E": [1],
    "F": [1, 1, 0, 1],
    "G": [0, 0, 1],
    "H": [1, 1, 1, 1],
    "I": [1, 1],
    "J": [1, 0, 0, 0],
    "K": [0, 1, 0],
    "L": [1, 0, 1, 1],
    "M": [0, 0],
    "N": [0, 1],
    "O": [0, 0, 0],
    "P": [1, 0, 0, 1],
    "Q": [0, 0, 1, 0],
    "R": [1, 0, 1],
    "S": [1, 1, 1],
    "T": [0],
    "U": [1, 1, 0],
    "V": [1, 1, 1, 0],
    "W": [1, 0, 0],
    "X": [0, 1, 1, 0],
    "Y": [0, 1, 0, 0],
    "Z": [0, 0, 1, 1],
    "1": [1, 0, 0, 0, 0],
    "2": [1, 1, 0, 0, 0],
    "3": [1, 1, 1, 0, 0],
    "4": [1, 1, 1, 1, 0],
    "5": [1, 1, 1, 1, 1],
    "6": [0, 1, 1, 1, 1],
    "7": [0, 0, 1, 1, 1],
    "8": [0, 0, 0, 1, 1],
    "9": [0, 0, 0, 0, 1],
    "0": [0, 0, 0, 0, 0],
  };

  static encode(String text) {
    String output = "";
    //get aschii decimal number from given string
    text.runes.forEach((int ascii) {
      if (ascii >= 97 && ascii <= 122) {
        ascii = ascii - 32;
      }

      String chr = new String.fromCharCode(ascii);
      output = output + listIntToMorseSymbol(_morseChar[chr]) + " ";
    });

    return output;
  }
  

  static decode(String morse) {
    String output = "";
    List<int> morsePatern = [];
    
    morse.runes.forEach((int ascii) {
      String chr = new String.fromCharCode(ascii);
      if (chr == dot) {
        morsePatern.add(1);
      } else if (chr == line) {
        morsePatern.add(0);
      } else {
        _morseChar.forEach((String key, List<int> value) {
          if (listEquals(morsePatern, value)) {
            output = output + key;
          }
        });
        morsePatern = [];
      }
    });
    
    //harus ada spasi dibelakang agar outputnya tampil
    return output;
  }

  static listIntToMorseSymbol(List<int> input) {
    String output = "";

    if (input == null) {
      return " " + " ";
    }

    input.forEach((int value) {
      if (value == 1) {
        output = output + dot;
      } else {
        output = output + line;
      }
    });

    return output;
  }
}
